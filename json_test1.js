let test = {
    name : 'hong',
    age : 12,
    hobby : ['computer', 'sleep'],
    job : {
        count: 2,
        list: [
            {
                name: '강사',
                company: 'line art'
            },
            {
                name: '편순이',
                company: 'GS25'
            },
        ]
    }
} // 객체
// C# 에서는 딕셔너리

test['job']["list"][1]['name']
let test2 = [] // list(배열)

// 컴파일 언어에서의 배열 : 값의 요소의 개수를 수정할 수 없다 => 그래서 여러개의 값을 넣을 때 list를 사용한다
// 요소의 개수가 정해져서 변하면 안되는 값 => [배열] list